<?php

namespace tests\Integration;

use pag\Connector\PasswordAuthenticationModule;
use pag\Connector\SftpClient;
use pag\TestCase;
use Symfony\Component\Dotenv\Dotenv;

class SftpClientTest extends TestCase
{
    /** @var string */
    private static $host;
    /** @var string */
    private static $port;
    /** @var string */
    private static $username;
    /** @var string */
    private static $password;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        $dotEnv = new Dotenv();
        $env = $dotEnv->parse(file_get_contents(__DIR__ . "/../../.env"));
        self::$host = $env['SSH_HOST'];
        self::$port = $env['SSH_PORT'];
        self::$username = $env['SSH_CREDENTIAL'];
        self::$password = $env['SSH_CREDENTIAL'];
    }

    public function testPasswordConnection()
    {
        # Given
        $sftp = new SftpClient();

        # When
        $lambda = function () use ($sftp) {
            $sftp->connect(
                self::$host,
                self::$port,
                new PasswordAuthenticationModule(
                    self::$username,
                    self::$password
                )
            );
        };

        # Then
        $this->assertNull($this->catchException($lambda));
    }

    public function testaze()
    {
        $sftp = new SftpClient();
        $sftp->connect(
            self::$host,
            self::$port,
            new PasswordAuthenticationModule(
                self::$username,
                self::$password
            )
        );
    }
}
