<?php

namespace tests\Unit;


use pag\Connector\ConnectorException;
use pag\Connector\NetrcParser;
use pag\TestCase;

class NetrcParserTest extends TestCase
{

    public function testCorrectConfigDoesntThrow()
    {
        # Given
        $lambda = function () {
            $this->makeCorrectParser();
        };

        # When
        $exception = $this->catchException($lambda);

        # Then
        $this->assertNull($exception);
    }

    public function makeCorrectParser()
    {
        return new NetrcParser("machine ftp.host.fr login login-value password password-value");
    }

    public function testMissingPasswordThrows()
    {
        # Given
        $lambda = function () {
            new NetrcParser("machine ftp.host.fr login login-value password");
        };

        # When
        $exception = $this->catchException(
            $lambda
        );

        # Then
        $this->assertEquals(
            new ConnectorException("Bad Syntax for netrc : Missing value token for Password"),
            $exception
        );
    }

    public function testGetCouple()
    {
        # Given
        $parser = $this->makeCorrectParser();

        # When
        list($username, $password) = $parser->getCouple('ftp.host.fr');

        # Then
        $this->assertEquals('login-value', $username);
        $this->assertEquals('password-value', $password);
    }

    public function testBadGetCouple()
    {
        # Given
        $parser = $this->makeCorrectParser();

        # When
        $exception = $this->catchException(
            function () use ($parser) {
                $parser->getCouple('bad domain');
            }
        );

        # Then
        $this->assertEquals(new ConnectorException("No Logins for host [bad domain]"), $exception);
    }
}
