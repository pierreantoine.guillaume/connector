<?php

namespace pag\Connector;

trait Ssh2Auth
{
    public function visitSsh2(Ssh2 $ssh2, $host, $port)
    {
        $connection = $this->ssh2Connect($host, $port);
        $this->ssh2Identify($connection);
        $this->checkFingerPrint($ssh2, $connection);

        return $connection;
    }

    /**
     * @param $host
     * @param $port
     *
     * @return resource
     */
    protected function ssh2Connect($host, $port)
    {
        $connection = ssh2_connect($host, $port);
        if (!$connection) {
            throw new ConnectorException("Could not connect to server");
        }

        return $connection;
    }

    /**
     * @param Ssh2 $ssh2
     * @param resource $connection
     */
    private function checkFingerPrint(Ssh2 $ssh2, $connection)
    {
        if (!$ssh2->hasFingerprint()) {
            return;
        }
        $fingerprint = ssh2_fingerprint($connection);
        if ($ssh2->getFingerprint() !== $fingerprint) {
            throw new ConnectorException(
                "UNKNOWN HOST FINGERPRINT = $fingerprint",
                Ssh2::UNKNOWN_FINGERPRINT
            );
        }
    }
}