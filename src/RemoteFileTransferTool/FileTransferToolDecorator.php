<?php

namespace pag\Connector\RemoteFileTransferTool;


use pag\Connector\RemoteFileTransferTool;

abstract class FileTransferToolDecorator implements RemoteFileTransferTool
{
    private $fileTransferTool;

    public function __construct(RemoteFileTransferTool $fileTransferTool)
    {
        $this->fileTransferTool = $fileTransferTool;
    }

    public function copyLocalToRemote($local, $remote)
    {
        $this->fileTransferTool->copyLocalToRemote($local, $remote);
    }

    public function copyRemoteToLocal($remote, $local)
    {
        $this->fileTransferTool->copyRemoteToLocal($remote, $local);
    }
}