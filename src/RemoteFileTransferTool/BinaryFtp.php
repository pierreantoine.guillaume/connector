<?php

namespace pag\Connector\RemoteFileTransferTool;


use pag\Connector\FtpClient;
use pag\Connector\RemoteFileTransferTool;

class BinaryFtp implements RemoteFileTransferTool
{
    private $ftp;

    public function __construct(FtpClient $ftp)
    {
        $this->ftp = $ftp;
    }

    public function copyLocalToRemote($local, $remote)
    {
        $this->ftp->put_binary($local, $remote);
    }

    public function copyRemoteToLocal($remote, $local)
    {
        $this->ftp->get_binary($remote, $local);
    }
}