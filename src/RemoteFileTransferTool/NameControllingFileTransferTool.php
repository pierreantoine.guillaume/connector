<?php

namespace pag\Connector\RemoteFileTransferTool;

use pag\Connector\RemoteFileTransferTool;

class NameControllingFileTransferTool extends FileTransferToolDecorator
{

    private $remoteDirectoryForInput;
    private $remoteDirectoryForOutput;

    public function __construct(
        RemoteFileTransferTool $fileTransferTool,
        $remoteDirectoryForInput,
        $remoteDirectoryForOutput
    ) {
        parent::__construct($fileTransferTool);
        $this->remoteDirectoryForInput  = $remoteDirectoryForInput;
        $this->remoteDirectoryForOutput = $remoteDirectoryForOutput;
    }

    public function copyLocalToRemote($local, $remote)
    {
        parent::copyLocalToRemote($local, $this->alterNameForOutput($remote));
    }

    private function alterNameForOutput($filename)
    {
        return $this->remoteDirectoryForOutput . basename($filename);
    }

    public function copyRemoteToLocal($remote, $local)
    {
        parent::copyRemoteToLocal($this->alterNameForInput($remote), $local);
    }

    private function alterNameForInput($filename)
    {
        return $this->remoteDirectoryForInput . basename($filename);
    }
}