<?php

namespace pag\Connector\RemoteFileTransferTool;


use pag\Connector\FtpClient;
use pag\Connector\RemoteFileTransferTool;

class AsciiFtp implements RemoteFileTransferTool
{
    private $ftp;

    public function __construct(FtpClient $ftp)
    {
        $this->ftp = $ftp;
    }

    public function copyLocalToRemote($local, $remote)
    {
        $this->ftp->put_ascii($local, $remote);
    }

    public function copyRemoteToLocal($remote, $local)
    {
        $this->ftp->get_ascii($remote, $local);
    }
}