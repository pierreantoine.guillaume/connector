<?php

namespace pag\Connector;


interface ConnectionClient
{
    /**
     * @param string $hostname
     * @param int $port
     */
    public function connect($hostname, $port, AuthenticationModule $authenticationModule);
}