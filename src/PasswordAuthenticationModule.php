<?php

namespace pag\Connector;

class PasswordAuthenticationModule implements AuthenticationModule
{
    use Ssh2Auth;

    private $username;
    private $password;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    function visitFtp(FtpClient $ftp, $host, $port)
    {
        $connection = ftp_connect($host, $port);
        if (!$connection) {
            throw new ConnectorException("FTP : Could not connect to $host:$port");
        }

        $this->ftp_login($connection);

        return $connection;
    }

    private function ftp_login($connection)
    {
        if (!ftp_login($connection, $this->username, $this->password)) {
            throw new ConnectorException("Could not identify user with username and password");
        }
    }

    function visitFtpSsl(FtpClient $ftp, $host, $port)
    {
        $connection = ftp_ssl_connect($host, $port);
        if (!$connection) {
            throw new ConnectorException("FTP : Could not connect to $host:$port");
        }
        $this->ftp_login($connection);

        return $connection;
    }

    private function ssh2Identify($connection)
    {
        if (!ssh2_auth_password($connection, $this->username, $this->password)) {
            throw new ConnectorException("Could not connect to remote host with password");
        }
    }
}