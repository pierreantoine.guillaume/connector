<?php

namespace pag\Connector;


interface RemoteFileTransferTool
{
    public function copyLocalToRemote($local, $remote);

    public function copyRemoteToLocal($remote, $local);
}