<?php

namespace pag\Connector;

interface Ssh2 extends ConnectionClient
{
    const UNKNOWN_FINGERPRINT = 10;

    public function connect($hostname, $port, AuthenticationModule $authenticationModule);

    public function hasFingerprint();

    public function getFingerprint();
}