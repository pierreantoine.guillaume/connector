<?php

namespace pag\Connector;

use function ssh2_auth_pubkey_file;

class PubkeyAuthenticationModule implements AuthenticationModule
{
    use Ssh2Auth;

    private $username;
    private $pubkey_file;
    private $privkey_file;

    /**
     * pubkey_authentication_module constructor.
     *
     * @param $username
     * @param $pubkey_file
     * @param $privkey_file
     */
    public function __construct($username, $pubkey_file, $privkey_file)
    {
        $this->username = $username;
        $this->pubkey_file = $pubkey_file;
        $this->privkey_file = $privkey_file;
    }


    public function visitFtp(FtpClient $ftp, $host, $port)
    {
        throw new ConnectorException("No Pubkey Authentication with FTP");
    }

    public function visitFtpSsl(FtpClient $ftp, $host, $port)
    {
        throw new ConnectorException("No Pubkey Authentication with FTP Secure");
    }

    private function ssh2Identify($connection)
    {
        if (!ssh2_auth_pubkey_file($connection, $this->username, $this->pubkey_file, $this->privkey_file)) {
            throw new ConnectorException("Could not connect to remote host with pubkey");
        }
    }
}