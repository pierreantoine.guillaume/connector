<?php

namespace pag\Connector;

interface AuthenticationModule
{
    public function visitFtp(FtpClient $ftp, $host, $port);

    public function visitFtpSsl(FtpClient $ftp, $host, $port);

    public function visitSsh2(Ssh2 $ssh2, $host, $port);
}