<?php

namespace pag\Connector;


class SftpClient implements Ssh2, RemoteFileTransferTool
{
    private $connection;
    private $sftp;
    private $fingerPrint;

    public function connect($hostname, $port, AuthenticationModule $authenticationModule, $fingerprint = null)
    {
        $this->fingerPrint = $fingerprint;
        $this->setConnection($authenticationModule->visitSsh2($this, $hostname, $port));
    }

    private function setConnection($connection)
    {
        $this->connection = $connection;
        $this->buildSftpConnection();
    }

    private function buildSftpConnection()
    {
        $this->sftp = ssh2_sftp($this->connection);
        if (!$this->sftp) {
            throw new ConnectorException("Could not initialize SFTP subsystem.");
        }
    }

    public function __destruct()
    {
        if ($this->connection) {
            $this->disconnect();
        }
    }

    public function disconnect()
    {
        if (!$this->connection) {
            return;
        }
        $this->connection = null;
        $this->sftp       = null;
    }

    public function getFingerprint()
    {
        if (!$this->hasFingerprint()) {
            throw new ConnectorException("No fingerprints");
        }

        return $this->fingerPrint;
    }

    public function hasFingerprint()
    {
        return !is_null($this->fingerPrint);
    }

    public function delete($filename)
    {
        if (!ssh2_sftp_unlink($this->sftp, $filename)) {
            throw new ConnectorException("Failed to delete file");
        }
    }

    public function chmod($filename, $mode)
    {
        if (!ssh2_sftp_chmod($this->sftp, $filename, $mode)) {
            throw new ConnectorException("Failed to change folder mode");
        }
    }

    public function symbolicLinkStat($path)
    {
        return ssh2_sftp_lstat($this->sftp, $path);
    }

    public function mkdir($dirname, $mode = 0744, $recursive = false)
    {
        if (!ssh2_sftp_mkdir($this->sftp, $dirname, $mode, $recursive)) {
            throw new ConnectorException("Failed to create directory");
        }
    }

    public function readlink($link)
    {
        return ssh2_sftp_readlink($this->sftp, $link);
    }

    public function realpath($filename)
    {
        return ssh2_sftp_realpath($this->sftp, $filename);
    }

    public function renameFromTo($from, $to)
    {
        if (!ssh2_sftp_rename($this->sftp, $from, $to)) {
            throw new ConnectorException("Failed to rename file");
        }
    }

    public function rmdir($dirname)
    {
        if (!ssh2_sftp_rmdir($this->sftp, $dirname)) {
            throw new ConnectorException("Fail to remove directory");
        }
    }

    public function stat($path)
    {
        return ssh2_sftp_stat($this->sftp, $path);
    }

    public function symlink($target, $link)
    {
        if (!ssh2_sftp_symlink($this->sftp, $target, $link)) {
            throw new ConnectorException("Fail to make symlink");
        }
    }

    public function copyLocalToRemote($local, $remote)
    {
        if (!$this->copyFile($local, $this->makeSsh2Link() . $remote)) {
            throw new ConnectorException("Failed to put local file on remote server");
        }
    }

    private function copyFile($source, $target)
    {
        $source_handler = fopen($source, 'r');
        $target_handler = fopen($target, 'w');

        $result = $writtenBytes = stream_copy_to_stream($source_handler, $target_handler);
        fclose($source_handler);
        fclose($target_handler);

        return $result;
    }

    private function makeSsh2Link()
    {
        $sftp = intval($this->sftp);

        return "ssh2.sftp://{$sftp}/";
    }

    public function copyRemoteToLocal($remote, $local)
    {
        if (!$this->copyFile($this->makeSsh2Link() . $remote, $local)) {
            throw new ConnectorException("Failed to get remote file");
        }
    }

    public function isDir($path)
    {
        return $this->isDir($this->makeSsh2Link() . $path);
    }

    public function read($filename)
    {
        return file_get_contents($this->makeSsh2Link() . $filename);
    }

    public function ls($string)
    {
        $file_descriptor = intval($this->sftp);
        return new SftpDirectoryIterator("ssh2.sftp://{$file_descriptor}", $string);
    }

    public function exec($string)
    {
        return ssh2_exec($this->connection, $string);
    }

}