<?php

namespace pag\Connector;

class FtpClientSslClient extends FtpClient
{
    public function connect($hostname, $port, AuthenticationModule $authenticationModule)
    {
        $this->setConnection($authenticationModule->visitFtpSsl($this, $hostname, $port));
        $this->setModePassive();
    }
}