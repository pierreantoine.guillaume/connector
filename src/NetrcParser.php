<?php

namespace pag\Connector;

class NetrcParser
{

    private $tokens;
    private $logins;
    private $passwords;
    private $currentIndex = 0;

    public function __construct($string)
    {
        $this->tokens = preg_split('#\s+#', $string);
        $this->parse();
    }

    private function parse()
    {
        $currentMachine = '';
        for (; $this->currentIndex < count($this->tokens); ++$this->currentIndex) {
            switch ($this->tokens[$this->currentIndex]) {
                case 'machine':
                    ++$this->currentIndex;
                    $this->assertLength("Missing value token for Machine");
                    $currentMachine = $this->tokens[$this->currentIndex];
                    break;
                case 'login':
                    ++$this->currentIndex;
                    $this->assertLength("Missing value token for Login");
                    $this->logins[$currentMachine] = $this->tokens[$this->currentIndex];
                    break;
                case 'password':
                    ++$this->currentIndex;
                    $this->assertLength("Missing value token for Password");
                    $this->passwords[$currentMachine] = $this->tokens[$this->currentIndex];
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param string $errorString
     */
    private function assertLength($errorString)
    {
        if ($this->currentIndex >= count($this->tokens)) {
            throw new ConnectorException("Bad Syntax for netrc : $errorString");
        }
    }

    public function getCouple($machine)
    {
        $this->assertKnowsMachine($machine);
        return [$this->logins[$machine], $this->passwords[$machine]];
    }

    private function assertKnowsMachine($machine)
    {
        if (!array_key_exists($machine, $this->logins)) {
            throw new ConnectorException("No Logins for host [$machine]");
        }
        if (!array_key_exists($machine, $this->passwords)) {
            throw new ConnectorException("No Passwords for host [$machine]");
        }
    }
}