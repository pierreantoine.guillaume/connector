.PHONY: build help shell start ssh_key ssh_pass
.SILENT:

include .env
include .env.local


export PRIVATE_KEY := $(shell cat ./docker/sshd/data/id_rsa)

dc = docker-compose
run = $(dc) run --user 1000:1000 --rm php
php =  $(run) php

start: build
	$(dc) up -d --remove-orphans

stop:
	$(dc) stop

down: stop
	$(dc) down

tests: start vendor
	$(run) vendor/bin/phpunit

help:  ## Displays this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

composer.lock: composer.json
	$(run) composer update --lock --no-scripts --no-interaction

vendor: composer.lock ## makes the vendors
	$(run) composer install

rebuild: docker/php/Dockerfile
	$(dc) build --no-cache

build: docker/php/Dockerfile
	$(dc) build

shell: build
	$(run) bash

ssh_key: start
	ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile /dev/null" -i docker/sshd/data/id_rsa ${SSH_CREDENTIAL}@localhost -p 6000

ssh_pass: start
	ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile /dev/null" ${SSH_CREDENTIAL}@localhost -p 6000

root: build
	$(dc) run --rm php bash

.env.local:
	touch .env.local